import * as Express from 'express';
// import {MainController} from './controllers';

const app: Express.Application = Express();

// app.use('/', MainController)

app.listen(3000, () => {
    console.log(`Application started`);
})